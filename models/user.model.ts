import { Schema, model, Document } from 'mongoose';
import bcrypt from 'bcrypt'

const userSchema = new Schema({
    name: {
        type: String,
        required: [true, 'name its required']
    },
    avatar: {
        type: String
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'email its required']
    },
    password: {
        type: String,
        required: [true, 'password its required']
    }
});

userSchema.method('checkPassword', function (password: string = ''): boolean {
    if (bcrypt.compareSync ( password, this.password)){
        return true
    }else{
        return false
    }
});

interface IUser extends Document{
    name: string;
    email: string;
    password: string;
    avatar: string;

    checkPassword(password: string): boolean;
}

export const User = model<IUser>('User', userSchema)