import {Response, NextFunction} from 'express'
import Token from '../class/token';

export const verifyJwt =  ((req: any, resp: Response, next: NextFunction) => {
    const userJwt = req.get('x-token') || '';

    Token.checkJwt( userJwt )
        .then( (decoded: any ) => {
            req.user = decoded.user;
            next();
        })
        .catch( err => {
            resp.json({
                ok: false,
                message: "invalid token"
            })
        })
    
});