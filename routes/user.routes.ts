import { Router, Request, Response } from "express";
import { User } from "../models/user.model";
import bcrypt from "bcrypt";
import Token from "../class/token";
import { verifyJwt } from "../middlewares/auth-middleware";

const userRoutes = Router();

userRoutes.post("/create", (req: Request, res: Response) => {
  const user = {
    name: req.body.name,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 10),
    avatar: req.body.avatar,
  };

  User.findOne({email: req.body.email}, (err, userDB) => {
    if (err) throw err;

    if(userDB) {
      return res.json({
        ok: false,
        message: "email/user already exists on DB"
      });
    }
  });

  User.create(user).then((userDB: any) => {

    const tokenUser = Token.getJwt({
      _id: userDB._id,
      name: userDB.name,
      email: userDB.email,
      avatar: userDB.avatar
    });

    res.json({
      ok: true,
      token: tokenUser  
    });
  });
});

userRoutes.post('/login', (req: Request, res: Response) => {
  const body = req.body;

  User.findOne({email: body.email}, (err, userDB) => {
      if (err) throw err;

      if(!userDB) {
        return res.json({
          ok: false,
          message: "Invalid credentials"
        });
      }

      if(userDB.checkPassword(body.password)){

        const tokenUser = Token.getJwt({
          _id: userDB._id,
          name: userDB.name,
          email: userDB.email,
          avatar: userDB.avatar
        });

        res.json({
          ok: true,
          token: tokenUser
        })
      }else{
        return res.json({
          ok: false,
          message: "Invalid credentials *"
        })
      }
  });
});

userRoutes.put('/update', [verifyJwt],  (req: any, res: Response) => {

  const user = {
    name: req.body.name,
    email: req.body.email,
    avatar:  req.body.avatar
  }

  User.findByIdAndUpdate( req.user._id, user, { new: true}, (err, userDB: any) => {
    if(err) throw err;

    if(!userDB){
      res.json({
        ok: false,
        message: "user does not exist on DB"
      });
    }

    const tokenUser = Token.getJwt({
      _id: userDB._id,
      name: userDB.name,
      email: userDB.email,
      avatar: userDB.avatar
    });

    res.json({
      ok: true,
      message: "successfully updated",
      token: tokenUser,
    });

  });

});

userRoutes.get('/', [verifyJwt], ( req: any, res: Response ) => {
  const user = req.user;

  res.json({
    ok:true,
    user
  })
})

export default userRoutes;
