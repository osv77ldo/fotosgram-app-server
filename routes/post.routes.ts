import { Router, Response, response } from "express";
import { verifyJwt } from "../middlewares/auth-middleware";
import { Post } from "../models/post.model";
import { FileUpload } from "../interfaces/file-upload.interface";
import FileSystem from "../class/file-system";

const postRoutes = Router();
const fileSystem = new FileSystem();

postRoutes.post("/", [verifyJwt], (req: any, res: Response) => {
  const body = req.body;

  body.user = req.user._id;

  const images = fileSystem.saveTempImagesToPost( body.user )
  body.images = images;

  Post.create(body)
    .then(async (postDB) => {
      await postDB.populate("user", "-password").execPopulate();

      res.json({
        ok: true,
        message: "success",
        post: postDB,
      });
    })
    .catch((err) => {
      res.json({
        ok: false,
        err,
      });
    });
});

postRoutes.get("/", async (req: any, res: Response) => {
  const page = Number(req.query.page) || 1;
  const skip = (page - 1) * 10;

  const posts = await Post.find()
    .sort({ _id: -1 })
    .limit(10)
    .skip(skip)
    .populate("user", "-password")
    .exec();

  res.json({
    ok: true,
    page,
    posts,
  });
});

postRoutes.post("/upload", [verifyJwt], async (req: any, res: Response) => {
  if (!req.files) {
    return res.status(400).json({
      ok: false,
      message: "Files are required",
    });
  }
  const file: FileUpload = req.files.image;

  if (!file) {
    return res.status(400).json({
      ok: false,
      message: "File are required",
    });
  }

  if (!file.mimetype.includes("image")) {
    return res.status(400).json({
      ok: false,
      message: "Invalid File",
    });
  }

  await fileSystem.saveImageIntoTempFolder(file, req.user._id);

  res.json({
    ok: true,
    file: file.mimetype,
  });
});

postRoutes.get('/image/:userid/:imgid', (req: any, res: Response) => {
  const userId = req.params.userid;
  const imgId = req.params.imgid;

  const pathImage = fileSystem.getUrlImage(userId, imgId);

  res.sendFile(pathImage);
});

export default postRoutes;
