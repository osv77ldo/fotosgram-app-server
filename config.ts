import * as dotenv from "dotenv";

dotenv.config();
let path;
if ( process.env.NODE_ENV !== "production") {
    path = `${__dirname}/../../.env`;
}
dotenv.config({ path: path });

export const PORT = process.env.PORT;
export const JWT_SECRET = process.env.JWT_SECRET;
