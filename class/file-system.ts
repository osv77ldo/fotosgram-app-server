import { FileUpload } from "../interfaces/file-upload.interface";
import path from "path";
import fs from "fs";
import uniqid from "uniqid";

export default class FileSystem {
  constructor() {}

  saveImageIntoTempFolder(file: FileUpload, userId: string) {
    return new Promise((resolve, reject) => {
      const path = this.createUserFolder(userId);

      const fileName = this.generateUniqueFileName(file.name);

      file.mv(`${path}/${fileName}`, (err: any) => {
        if (err) reject(err);
        resolve();
      });
    });
  }

  private createUserFolder(userId: string) {
    const pathUser = path.resolve(__dirname, "../uploads/", userId);
    const pathUserTemp = pathUser + "/temp";

    const exists = fs.existsSync(pathUser);

    if (!exists) {
      fs.mkdirSync(pathUser);
      fs.mkdirSync(pathUserTemp);
    }

    return pathUserTemp;
  }

  private generateUniqueFileName(fileName: String) {
    const nameArr = fileName.split(".");
    const extension = nameArr[nameArr.length - 1];

    const idUnique = uniqid();

    return `${idUnique}.${extension}`;
  }

  saveTempImagesToPost( userId: string) {
    const pathTemp = path.resolve(__dirname, "../uploads/", userId, 'temp');
    const pathPost = path.resolve(__dirname, "../uploads/", userId, 'post');
  
    if(!fs.existsSync(pathTemp)){
      return[]
    }
  
    if(!fs.existsSync(pathPost)){
      fs.mkdirSync(pathPost);
    }

    const imagesTemp = this.getTempImages(userId);

    imagesTemp.forEach( image => {
      fs.renameSync(`${ pathTemp}/${image}`,`${ pathPost}/${image}`);
    });

    return imagesTemp;
  }

  private getTempImages( userId: string ){
    const pathTemp = path.resolve(__dirname, "../uploads/", userId, 'temp');

    return fs.readdirSync(pathTemp) || [];

  }

  getUrlImage(userId: string, imgId: string){
    const pathImage = path.resolve( __dirname, '../uploads', userId, 'post', imgId);

    const existsImg = fs.existsSync(pathImage);

    if(!existsImg){
      return path.resolve( __dirname, '../assets/404.jpg');
    }

    return pathImage;

  }
}

