import jwt from 'jsonwebtoken';
import { JWT_SECRET } from '../config'

export default class Token {
    private static seed: any = JWT_SECRET;
    private static expiresIn: string = '1d';

    constructor(){}

    static getJwt( payload: any): string {

        return jwt.sign({
            user: payload
        }, this.seed, { expiresIn: this.expiresIn});

    }

    static checkJwt( userJwt: string ) {

        return new Promise( (resolve, reject) => {

            jwt.verify( userJwt, this.seed, (err: any, decoded: any ) => {
                
                if(err){
                    reject();
                }else{
                    resolve( decoded );
                }
            })

        });
    }
}