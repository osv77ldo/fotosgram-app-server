import Server from './class/server';
import mongoose  from 'mongoose';
import bodyParser from 'body-parser';
import fileUpload from 'express-fileupload'
import userRoutes from './routes/user.routes';
import postRoutes from './routes/post.routes';
import cors from 'cors'

const server = new Server();

// mdw
server.app.use( bodyParser.urlencoded({ extended: true }));
server.app.use(bodyParser.json());
server.app.use( fileUpload() );
server.app.use(cors( {origin:true, credentials:true}))


//routes 
server.app.use('/user', userRoutes);
server.app.use('/post', postRoutes);

//MongoDB
mongoose.connect('mongodb://localhost:27017/fotosgram', 
                    { useNewUrlParser: true, useCreateIndex: true }, (err: any) => {
                        if (err) throw err;

                        console.log('Database succesfully connected');
                    });

server.start( () => {
    console.log(`server running on port ${server.port}`);
});